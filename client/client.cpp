#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <iostream>
#include <cstring>
#include <signal.h>

#define PORT    7777
#define MAX_MESSAGE_LENGTH 1024

volatile sig_atomic_t stopSignal = 0;

void inputName(char * name);
int initSocket();
void writeMessage(int sock);
void showMenu(int sock);
void sigint(int sig);

int main()
{
    int sock = initSocket();

    char name[32];
    inputName(name);
    send(sock, name, sizeof(name), 0);

    signal(SIGINT, sigint);
    
    char lastMessage[MAX_MESSAGE_LENGTH] = "";
    while(1)
    {
        if (stopSignal)
        {
            showMenu(sock);
        }
        else
        {
            char buf[MAX_MESSAGE_LENGTH];
            int bytes_read = recv(sock, buf, sizeof(buf), MSG_DONTWAIT);
            buf[bytes_read] = '\0';
            if (strcmp(lastMessage, buf))
            {
                printf("%s\n", buf);
                strcpy(lastMessage, buf);
            }   
        }
    }

    return 0;
}

void writeMessage(int sock)
{
    printf("Input message:\n");
    char * message = NULL;
    size_t len = 0;
    getline(&message, &len, stdin);
    message[strlen(message) - 1] = '\0'; // remove \n in the end of string.
    send(sock, message, len, 0);
    free(message);
}

void showMenu(int sock)
{
    int action = -1;
    
    printf("\nMenu:\n");
    printf("1. Listen messages\n");
    printf("2. Write message\n");
    printf("3. Exit\n");
    printf("Choose action: \n");
    
    scanf("%i", &action);
    getchar();

    switch(action)
    {
        case 1:
        {
            printf("Listening messages:\n");
            break;
        }
        case 2:
        {
            writeMessage(sock);
            break;
        }
        case 3:
        {
            printf("Disconnected from chat!\n");
            close(sock);
            exit(0);
            break;
        }
    }
    stopSignal = 0;
}

int initSocket()
{
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0)
    {
        perror("Error while creating socket!");
        exit(1);
    }

    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(PORT);
    addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    if (connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("Error while connection!");
        exit(2);
    }

    return sock;
}

void inputName(char * name)
{
    printf("Input name:\n");
    std::scanf("%s", name);
    getchar();
}

void sigint(int sig)
{
    stopSignal = 1;
}