#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <unistd.h>
#include <fcntl.h>
#include <cstring>
#include <ctime>

using std::vector;

#define PORT    7777

#define MAX_MESSAGE_LENGTH 1024

struct users_struct 
{
    int socketID;
    char name[32];
};

int getUsersMaxSocketID(vector<users_struct> vector);
void sendChatMessage(vector<users_struct> * users, char * msg, int length, int exceptSocket = -1);
void strdel(char *str, int length, int num);
void initSocket(int * listener);
void addNewConnection(vector<users_struct> *users, int socketID, char * name);
void removeConnection(vector<users_struct> *users, int num);
void recieveMessage(vector<users_struct> * users, int num, char * text);
void getTimeAsText(char * buf, int length);

int main()
{
    int listener;
    initSocket(&listener);

    vector<users_struct> users;
    users.clear();
    timeval timeout;
    timeout.tv_sec = 15;
    timeout.tv_usec = 0;

    printf("Server started!\n");

    while (1)
    {
        fd_set set;
        FD_ZERO(&set);
        FD_SET(listener, &set);

        for (unsigned int i = 0; i < users.size(); i++)
            FD_SET(users[i].socketID, &set);
        
        int max = std::max(listener, getUsersMaxSocketID(users));
        select(max + 1, &set, NULL, NULL, &timeout);

        if (FD_ISSET(listener, &set))
        {
            // new connection
            int sock = accept(listener, NULL, NULL);
            char buf[MAX_MESSAGE_LENGTH];
            recv(sock, buf, sizeof(buf), 0);
            fcntl(sock, F_SETFL, O_NONBLOCK);

            addNewConnection(&users, sock, buf);
        }

        for (unsigned int i = 0; i < users.size(); i++)
        {
            if (FD_ISSET(users[i].socketID, &set))
            {
                char buf[MAX_MESSAGE_LENGTH];
                int bytes_read = recv(users[i].socketID, buf, sizeof(buf), 0);

                if (bytes_read <= 0)
                {
                    // disconnect
                    removeConnection(&users, i);
                    continue;
                }

                // message
                recieveMessage(&users, i, buf);
            }
        }
    }

    return 0;
}

int getUsersMaxSocketID(vector<users_struct> vector)
{
    if (!vector.size())
        return 0;
    int result = vector[0].socketID;
    for (unsigned int i = 1; i < vector.size(); i++)
        if (vector[i].socketID > result)
            result = vector[i].socketID;

    return result;
}

void sendChatMessage(vector<users_struct> * users, char * msg, int length, int exceptSocket)
{
    for (unsigned int j = 0; j < (*users).size(); j++)
    {
        if ((*users)[j].socketID != exceptSocket)
            send((*users)[j].socketID, msg, length, 0);
    }
}

void strdel(char *str, int length, int num)
{
    for (int i = num; i < length - 1; i++)
        str[i] = str[i + 1];
    
    str[length] = '\0';
}

void initSocket(int * listener)
{
    *listener = socket(AF_INET, SOCK_STREAM, 0);
    if (*listener < 0)
    {
        perror("Error while creating socket!");
        exit(1);
    }

    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(PORT);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(*listener, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("Error while binding socket!");
        exit(2);
    }

    listen(*listener, 1);
}

void addNewConnection(vector<users_struct> *users, int socketID, char * name)
{
    printf("User %s connected to server.\n", name);
    users_struct user;
    user.socketID = socketID;
    strcpy(user.name, name);

    char message[MAX_MESSAGE_LENGTH];
    snprintf(message, sizeof(message), "Hello, %s. You successfully connected to chat!", user.name);
    send(user.socketID, message, sizeof(message), 0);
    snprintf(message, sizeof(message), "User %s connected to chat!", user.name);
    sendChatMessage(users, message, sizeof(message), socketID);
    
    users->push_back(user);
}

void removeConnection(vector<users_struct> *users, int num)
{
    char message[MAX_MESSAGE_LENGTH];
    snprintf(message, sizeof(message), "User %s disconnected from chat!", (*users)[num].name);
    sendChatMessage(users, message, strlen(message), (*users)[num].socketID);
    printf("User %s disconnected from server.\n", (*users)[num].name);
    close((*users)[num].socketID);
    users->erase(users->begin() + num);
}

void recieveMessage(vector<users_struct> * users, int num, char * text)
{
    printf("Message from %s[%i]: %s\n", (*users)[num].name, (*users)[num].socketID, text);

    char timeText[60];
    getTimeAsText(timeText, sizeof(timeText));

    char message[1024];
    snprintf(message, sizeof(message), "[%s] %s: %s", timeText, (*users)[num].name, text);
    sendChatMessage(users, message, strlen(message), (*users)[num].socketID);
    snprintf(message, sizeof(message), "[%s] You: %s", timeText, text);
    send((*users)[num].socketID, message, sizeof(message), 0);
}

void getTimeAsText(char * buf, int length)
{
    time_t rawtime;
    time(&rawtime);
    strftime(buf, length, "%H:%M:%S", localtime(&rawtime));
}